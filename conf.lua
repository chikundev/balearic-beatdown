-- chikun :: 2015
-- Configuration file

require("flags")

function love.conf(game)

	-- Attach a console for Windows debug [DISABLE ON DISTRIBUTION]
	game.console    = true

	-- Version of LÖVE which this game was made for
	game.version    = "0.9.2"

	-- Omit modules due to disuse
	game.modules.math   = false
	game.modules.thread = false

	-- Various window settings
	game.window.title   = "Balearic Beatdown"
	game.window.width   = GAME_WIDTH * DEFAULT_SCALE
	game.window.height  = GAME_HEIGHT * DEFAULT_SCALE
	game.window.resizable = true
end
