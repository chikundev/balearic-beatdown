return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.12.3",
  orientation = "orthogonal",
  width = 10,
  height = 10,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "sexy_halloween",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/sexy_halloween.jpg",
      imagewidth = 480,
      imageheight = 282,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 10,
      height = 10,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 35, 36, 0, 0, 0, 0, 0,
        0, 0, 0, 50, 51, 0, 0, 0, 0, 0,
        0, 0, 21, 22, 23, 24, 21, 22, 23, 24,
        21, 22, 36, 37, 38, 39, 36, 37, 38, 39,
        36, 37, 51, 52, 53, 54, 51, 52, 53, 54,
        51, 52, 66, 67, 68, 69, 66, 67, 68, 69,
        66, 67, 68, 69, 0, 0, 0, 0, 0, 0
      }
    }
  }
}
