-- chikun :: 2014-2015
-- Loads all states from the current folder and creates control functions


-- State management functions --

-- Create state table
local state = {
	current = nil
}


--[[
	Kill old state and load a new one.
	INPUT:  State to change to.
	OUTPUT: Nothing.
]]
function state.change(new_state)

	states[state.current]:kill()
	state.load(new_state)
end


--[[
	Loads new state. Reloads current state if argument omitted.
	INPUT:  New state to load, or nothing.
	OUTPUT: Nothing.
]]
function state.load(new_state)

	state.current = new_state or state.current
	states[state.current]:create()
end


--[[
	Kill old state and sets another.
	INPUT:  State to move to.
	OUTPUT: Nothing.
]]
function state.set(new_state)

	states[state.current].kill()
	state.current = new_state
end


--[[
	Update current state.
	INPUT:  Delta time.
	OUTPUT: Nothing.
]]
function state.update(dt, given_state)

	states[given_state or state.current]:update(dt)
end


--[[
	Draw current state.
	INPUT:  Nothing.
	OUTPUT: Nothing.
]]
function state.draw(given_state)

	states[given_state or state.current]:draw()
end



-- State prototype --

-- State class on which all others are based
PrototypeState = class(function(new_class) end)

function PrototypeState:create() end    -- On state create
function PrototypeState:update(dt) end  -- On state update, has delta time
function PrototypeState:draw() end      -- On state draw
function PrototypeState:kill() end      -- On state kill



-- State loading --

-- Table in which to hold all states
states = { }


-- Get a table of files / subdirs from dir
local items = love.filesystem.getDirectoryItems(...)


-- Iterate through all files and load them into states
for key, val in ipairs(items) do

	-- Load file as long as it isn't init.lua
	if (val ~= "init.lua") then

		local name = val:sub(1, -5)
		states[name] = require(... .. "/" .. name)
	end
end

return state
