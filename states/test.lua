-- chikun :: 2014-2015
-- Test state


-- Temporary state, removed at end of script
local TestState = class(PrototypeClass, function(new_class) end)


-- On state create
function TestState:create()

	self.rotation = 0

	lg.setFont(lg.newFont(18))
end


-- On state update
function TestState:update(dt)

	self.rotation = self.rotation + dt
end


-- On state draw
function TestState:draw()

	lg.setColor(200, 50, 50)

	lg.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	lg.setColor(255, 255, 255)

	lg.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)

	cg.drawCentred(gfx.sexy_halloween, 256, 256, self.rotation, 0.8)
end


-- On state kill
function TestState:kill() end


-- Transfer data to state loading script
return TestState
